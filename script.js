setInterval(() => {
  clock.textContent = new Intl.DateTimeFormat("en-US", {
    hourCycle: "h24",
    hour: "2-digit",
    minute: "2-digit",
    second: "2-digit",
  }).format(Date.now());
}, 1000);
